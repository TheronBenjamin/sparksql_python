# Importer SparkSession
from pyspark.sql import SparkSession
from pyspark.sql.types import StructType, StructField, IntegerType, StringType

def parse_line(line):
    # Diviser la ligne en champs
    fields = line.split(",")

    # Extraire les champs nécessaires avec les types spécifiés
    duration = int(fields[0])
    protocol_type = fields[1]
    service = fields[2]
    flag = fields[3]
    src_bytes = int(fields[4])
    dst_bytes = int(fields[5])
    label = fields[41]  # Champ label

    # Retourner les champs sous forme de tuple
    return (duration, protocol_type, service, flag, src_bytes, dst_bytes, label)


def get_label_type(label):
    if label != "normal.":
        return "attack"
    else:
        return "normal"


# Créer une SparkSession
spark = SparkSession.builder \
    .appName("Chargement fichier dans RDD") \
    .getOrCreate()

# QUESTION 1 - Chemin vers le fichier kddcup.data_10_percent.gz
file_path = "data/kddcup.data_10_percent.gz"

# Charger le fichier dans un RDD
data_rdd = spark.sparkContext.textFile(file_path)

# Afficher le nombre de lignes dans le RDD
print("Nombre de lignes dans le RDD :", data_rdd.count())

# Afficher quelques lignes du RDD pour vérification
for line in data_rdd.take(5):
    print(line)

# Appliquer la fonction map pour convertir chaque ligne en tuple de champs avec les types spécifiés
parsed_data_rdd = data_rdd.map(parse_line)

# QUESTIONS 2 & 3 Afficher les premières lignes pour vérification
for line in parsed_data_rdd.take(5):
    print(line)
    print(get_label_type(line[6]))  # Accessing the label field by index

# QUESTION 4
schema = StructType([
    StructField("duration", IntegerType(), True),
    StructField("protocol_type", StringType(), True),
    StructField("service", StringType(), True),
    StructField("flag", StringType(), True),
    StructField("src_bytes", IntegerType(), True),
    StructField("dst_bytes", IntegerType(), True),
    StructField("label", StringType(), True)
])

# Convertir le RDD en DataFrame en spécifiant le schéma
df = spark.createDataFrame(parsed_data_rdd, schema)

# QUESTION - Créer une vue temporaire à partir du DataFrame
df.createOrReplaceTempView("interactions")

# Afficher les premières lignes du DataFrame
df.show(5)

# QUESTION 6. Afficher les interactions du protocole tcp ayant une durée plus qu’une seconde et sans
# transfert à partir de la destination (dst_bytes = 0).
print("Interactions du protoc")
# Exécuter une requête SQL pour filtrer les interactions
result = spark.sql("""
    SELECT * 
    FROM interactions 
    WHERE protocol_type = 'tcp' 
    AND duration > 1 
    AND dst_bytes = 0
""")

# Afficher les résultats
result.show()


# QUESTION 7. Afficher le nombre d’interaction par type de protocole.
print("Afficher le nombre d’interaction par type de protocole.")
# Exécuter une requête SQL pour compter le nombre d'interactions par type de protocole
result = spark.sql("""
    SELECT protocol_type, COUNT(*) AS count
    FROM interactions
    GROUP BY protocol_type
""")

# Afficher les résultats
result.show()


# QUESTION 8. Afficher le nombre de données par type attaque.

print(" Afficher le nombre de données par type attaque.")
# Exécuter une requête SQL pour compter le nombre de données par type d'attaque
result = spark.sql("""
    SELECT label, COUNT(*) AS count
    FROM interactions
    GROUP BY label
""")

# Afficher les résultats
result.show()

# Arrêter la SparkSession
spark.stop()
